<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
//use Laracasts\TestDummy\Factory as TestDummy;

class TipoUsuarioTableSeeder extends Seeder
{
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        $tabla="ad_tipousuario";
        DB::table($tabla)->truncate();
        $campos = array(
			'tipo_usuario'=>'ADMINISTRADOR'
		);		
	DB::table($tabla)->insert($campos); 
    }
}
