<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
//use Laracasts\TestDummy\Factory as TestDummy;

class UsuariosTableSeeder extends Seeder
{
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        $tabla="users";
        DB::table($tabla)->truncate();
        $campos = array(
			'name'=>'Administrador',
                        'email'=>'admin@admin.com',
			'password'=>bcrypt('admin'),
                        'cedula'=>'0000000000',
                        'telefono'=>'0000000000',
                        'fecha_nacimiento'=>date("Y-m-d")
		);		
	DB::table($tabla)->insert($campos);        
    }
}
