<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
//use Laracasts\TestDummy\Factory as TestDummy;

class PerfilTableSeeder extends Seeder
{
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        $tabla="ad_perfil";
        DB::table($tabla)->truncate();
        $campos = array(
			'perfil'=>'GENERAL'
		);		
	DB::table($tabla)->insert($campos); 
    }
}
