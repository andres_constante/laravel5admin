<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposFieldToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {                        
            $table->string('cedula',100)->unique();
            $table->string('telefono',15)->default('0000000000');
            $table->date("fecha_nacimiento");            
            $table->integer('id_tipo_usuario')->default(1);            
            $table->integer('id_perfil')->default(0)->nullable(); 
            $table->integer('nuevo')->default(1);
            $table->string('estado')->default('ACT');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {                        
            $table->dropColumn('cedula');
            $table->dropColumn('telefono');
            $table->dropColumn("fecha_nacimiento");
            $table->dropColumn('id_tipo_usuario');
            $table->dropColumn('id_perfil');
            $table->dropColumn('nuevo');
            $table->dropColumn('estado');
        });
    }
}
