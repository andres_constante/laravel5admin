<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->decimal('nota1')->default(0)->nullable();
            $table->decimal('nota2')->default(0)->nullable();
            $table->decimal('nota3')->default(0)->nullable();
            $table->decimal('nota4')->default(0)->nullable();
            $table->decimal('suma')->default(0);
            $table->decimal('promedio')->default(0);
            $table->string('observacion')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notas');
    }
}
