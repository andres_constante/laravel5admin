<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('menu');
            $table->string('ruta');
            $table->string('iconfaw',45);
            $table->integer('nivel');
            $table->integer('idmain');
            $table->decimal('orden',10,2);
            $table->string('adicional');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ad_menu');
    }
}
