<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdAuditoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('ad_auditoria', function(Blueprint $table) {
			$table->increments('id');
			$table->integer("idusuario");
                        $table->string("urlmenu");
                        $table->string("accion");
                        $table->string("ip");
                        $table->string("nompc");
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('ad_auditoria');
    }
}
