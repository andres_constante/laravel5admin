<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEncuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encuesta', function (Blueprint $table) {
            $table->increments('id');
            $table->string("cedula");            
            $table->string("nombres");
            $table->integer("edad");
            $table->string("residencia");
            $table->date("fecha");
            $table->string("organización");
            $table->string("tiporeciclaje");
            $table->string("tiporeciclador");
            $table->string("recicladoractivo");
            $table->string("carnetreciclador");
            $table->string("productoobj");
            $table->string("volumen");
            $table->string("horas");
            $table->string("precio");
            $table->string("vacunatetano");
            $table->integer("dosis");
            $table->string("tiempo");
            $table->string("otrasvacunas");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('encuesta');
    }
}
