<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Hay que agregar las siguientes referencias
use DB;
use \Illuminate\Support\Facades;
//use \App;
use MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class NotasIngresoController extends Controller
{
    var $textos=array("Ingreso de Notas","notasingreso","crear");
        var $camposcaption= array("Apellidos y Nombres","Actuación","Tareas","Lección","Evaluación","Total","Promedio","Observación");
        var $camposfield= array("nombres","nota1","nota2","nota3","nota4","suma","promedio","observacion");
        var $campostype=array("text","input","input","input","input","input-disabled","input-disabled","text");
            //Para chosen// Clase y multiple
        var $multiple=array("");
        var $clase=array("");
            //                        
        var $lista=array(array());
    public function __construct() {
        $this->middleware('auth');
    }     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla= \App\NotasModel::orderby("nombres")->get();
        return view('index_notas.mainindex',[
                "tabla"=>$tabla,
                "textos"=>$this->textos,
                "camposcaption"=>$this->camposcaption,
            "camposfield"=>$this->camposfield,
            "campostype"=>$this->campostype
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
