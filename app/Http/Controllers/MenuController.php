<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Hay que agregar las siguientes referencias
use DB;
use \Illuminate\Support\Facades;
//use \App;
use MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class MenuController extends Controller
{   
    //Variables Principales
    	var $textos=array("Menú de Opciones","menu","crear");
        var $camposcaption= array("Nombre Menú","Ruta / Url","Icono (FAW)","Nivel","ID Padre","Orden","Opciones");
        var $camposfield= array("menu","ruta","iconfaw","nivel","idmain","orden","adicional");
        var $campostype=array("text","text","text","select","select","text","text");
            //Para chosen// Clase y multiple
        var $multiple=array("","","","","","","");
        var $clase=array("","","","","","","");
            //                        
        var $lista=array(array(),array(),array(),array(1=>"1",2=>"2"),array(),array(),array());
    //
    public function __construct() {
        $this->middleware('auth');        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla= \App\MenuModel::all(); 
        return view('index.mainindex',["tabla"=>$tabla,"textos"=>$this->textos,
                "camposcaption"=>$this->camposcaption,"camposfield"=>$this->camposfield]);
    }
    public function guardar($id)
    {            
            $ruta=$this->textos[1];
            $msg="Registro Creado Exitosamente...!";
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new \App\MenuModel;
                $msgauditoria="Registro de Menú de Opciones";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= \App\MenuModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Actualizar Menú de Opciones";
            }
            $input=Input::all();
            $validator = Validator::make($input, \App\MenuModel::rules($id));
            /*echo "<pre>";
            print_r($input);die();*/
            //die($ruta);
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                //die("Hola");
                 foreach($this->camposfield as $keycam => $valuecam)
                 {
                    $cadena="\$guardar->".$valuecam." = Input::get('$valuecam');";
                    eval($cadena);                                   
                 }                                               
                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($this->camposfield[0]));
                 //MisFunciones::auditoria("Usuarios Nuevo: ",Input::get('name'));
                // redirect
			Session::flash('message', $msg);
			return Redirect::to($this->textos[1]);
            } 
            
        }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        	$menu=DB::table("menu")->where("nivel",1)->orderby("orden")->lists("menu","id");
        	if($menu)
 				$this->lista[4]=$menu;
 			else
 				$this->lista[4]=array(1 => 1);

            return \View::make('index.maincreate')        
                ->with("textos",$this->textos)
                ->with("camposcaption",$this->camposcaption)
                ->with("camposfield",$this->camposfield)
                ->with("campostype",$this->campostype)
                ->with("lista",$this->lista)
                ->with("multiple",$this->multiple)
                ->with("clase",$this->clase); 
            //return View::make('departamentos.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tabla = \App\MenuModel::find($id);
            /*  echo "<pre>";
            print_r($tabla);die();*/
            $titucampo=$this->textos[0].":";            
            return \View::make('index.mainshow')
                    ->with("tabla",$tabla)
                ->with("textos",$this->textos)
                ->with("camposcaption",$this->camposcaption)
                ->with("camposfield",$this->camposfield)                
                ->with("titucampo",$titucampo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
            $this->lista[4]=DB::table("menu")->where("nivel",1)->orderby("orden")->lists("menu","id");
            $tabla = \App\MenuModel::find($id);
            $this->textos[2]="editar";
            /**/
            return \View::make('index.maincreate') 
                ->with("tabla",$tabla)
                ->with("textos",$this->textos)
                ->with("camposcaption",$this->camposcaption)
                ->with("camposfield",$this->camposfield)
                ->with("campostype",$this->campostype)
                ->with("lista",$this->lista)
                ->with("multiple",$this->multiple)
                ->with("clase",$this->clase);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
