<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Hay que agregar las siguientes referencias
use DB;
use \Illuminate\Support\Facades;
//use \App;
use MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class PermisosAsignarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //        
        $tabla= \App\MenuModel::whereIn("id",function($query) use($id){
                $query->select('idmenu')
                ->from(with(new \App\PermisosUsuariosModel)->getTable())                
                ->where('idusuario', $id);
            })->orderby("orden")->get();
            /*echo "<pre>";
            print_r($tabla);
            die();*/
            //var_dump($menupermiso);
            // show the view and pass the nerd to it
            return view('indextree.mainshow',[
                    "tabla"=>$tabla,
                    "total"=>$tabla->count(),
                    "idmainper"=>0,
                    "cual"=>0
                    ]);    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //        
        $tabla= \App\MenuModel::orderby("orden")->get();
            /*echo "<pre>";
            print_r($tabla);
            die();*/
            //var_dump($menupermiso);
            // show the view and pass the nerd to it
        
        return view('indextree.mainshow',[
                    "tabla"=>$tabla,
                    "total"=>$tabla->count(),
                    "idmainper"=>$id,
                    "cual"=>0
                    ]);      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
