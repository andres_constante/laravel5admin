<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Hay que agregar las siguientes referencias
use DB;
use \Illuminate\Support\Facades;
//use \App;
use MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class AuditoriaController extends Controller
{
    var $textos=array("Auditoría de Usuarios","auditoria","crear");
        var $camposcaption= array("Usuario","Acción","URL","IP","PC","Fecha");
        var $camposfield= array("idusuario","accion","urlmenu","ip","nompc","created_at");
        var $campostype=array("select","text","text","text","text","text");
            //Para chosen// Clase y multiple
        var $multiple=array("");
        var $clase=array("");
            //                        
        var $lista=array(array());
    public function __construct() {
        $this->middleware('auth');        
    }        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
            $tabla=DB::table("ad_auditoria as a")
                    ->join("users as b","a.idusuario","=","b.id")
                    ->select("a.*","b.name")
                    ->orderby("a.created_at")
                    ->get();
            $this->camposfield[0]="name";
            return view('index.mainindex',[
                    "tabla"=>$tabla,
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield
                    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
