<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Hay que agregar las siguientes referencias
use DB;
use \Illuminate\Support\Facades;
//use \App;
use MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class PerfilPermisosController extends Controller
{
    var $textos=array("Permisos por Perfiles","perfilpermisos","crear","perfilpermisosasignar");
        var $camposcaption= array("Perfil");
        var $camposfield= array("idperfil");
        var $campostype=array("select");
            //Para chosen// Clase y multiple
        var $multiple=array('');
        var $clase=array();
            //                        
        var $lista=array(array());
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla= \App\PerfilPermisosModel::all(); 
            $perfiles= \App\PerfilModel::where("estado","ACT")->lists("perfil","id")->all();
            $this->lista[0]=$perfiles;
            return view('indextree.mainindex',[
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype,
                    "lista"=>$this->lista,
                    "multiple"=>$this->multiple,
                    "clase"=>$this->clase
                    ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tabla= \App\PerfilPermisosModel::all(); 
            $perfiles= \App\PerfilModel::where("estado","ACT")->lists("perfil","id")->all();
            $this->lista[0]=$perfiles;
            return view('indextree.maincreate',[
                    "tabla"=>$tabla,
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype,
                    "lista"=>$this->lista,
                    "multiple"=>$this->multiple,
                    "clase"=>$this->clase
                    ]); 
    }
        public function guardar($id)
        {            
            $ruta=$this->textos[1];
            $msg="Permisos asignados Exitosamente...!";
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new \App\PerfilPermisosModel;
            }
            else{
                $ruta.="/$id/edit";
                $guardar= \App\PerfilPermisosModel::find($id);
                $msg="Permisos Actualizados Exitosamente...!";
            }
            $input=Input::all();
            $validator = Validator::make($input, \App\PerfilPermisosModel::rules($id));
            /*echo "<pre>";
            print_r($input);die();*/
            //die($ruta);
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                //die($ruta);
                        $sw=0;
			$permimenu = Input::get('idmenumain');
                        $usuario=Input::get('idperfil');
                        /*echo "<pre>";
                        echo "Usuario: $usuario <br>";
                        print_r($permimenu);
                        die();*/
                        DB::table('ad_perfilpermisos')->where('idperfil', '=', $usuario)->delete();
                        //print_r($permimenu);
//                        if(is_array($permimenu))
  //                      {
                            foreach ($permimenu as $valor) {
                                echo "Menu:  $valor <br>";
                                $permisos = new \App\PerfilPermisosModel;
                                $permisos->idperfil  = $usuario;
                                $permisos->idmenu     = $valor;
                                $verpa= DB::table('ad_perfilpermisos')->where(function($query)use($valor,$usuario){
                                    $query->where("idperfil",$usuario)->where("idmenu",$valor);
                                })->first();
                                if(!$verpa)
                                {
                                    $permisos->save();
                                }
                                $sw=1;
                                /*Ver padre del hijo*/
                                $verpa= DB::table('menu')->where("id",$valor)->first();
                                $idpa=$verpa->idmain;
                                $verpa= DB::table('ad_perfilpermisos')->where(function($query)use($idpa,$usuario){
                                    $query->where("idperfil",$usuario)->where("idmenu",$idpa);
                                })->first();
                                if(!$verpa)
                                {
                                    $permisos = new \App\PerfilPermisosModel;
                                    $permisos->idperfil  = $usuario;
                                    $permisos->idmenu     = $idpa;
                                    
                                    $permisos->save();
                                }
                            }
                            //Actualizar por permisos asignados a usuarios con este perfil
                            if($sw==1)
                            {
                                //Permisos por Perfil
                                $perfil= Input::get('idperfil');
                                if($perfil)
                                {                                       
                                    DB::table('ad_menuusuario')->where('idusuario', function($query) use($perfil)
                                        {
                                            $query->select('id')
                                                ->from(with(new \App\UsuariosModel)->getTable())
                                                ->where('id_perfil',$perfil);
                                        })->delete();
                                        $user= \App\UsuariosModel::where('id_perfil',$perfil)->get();
                                        foreach ($user as $key => $value) {
                                            $permiper= \App\PerfilPermisosModel::where("idperfil",$perfil)
                                                ->select(array('idmenu',DB::raw($value->id. " as idusuario")));
                                            $bindings = $permiper->getBindings();
                                            $insert = 'INSERT into ad_menuusuario (idmenu,idusuario) '
                                                    . $permiper->toSql();
                                            DB::insert($insert, $bindings);
                                        }
                                }
                                Auditoria("Asignación de Permisos a Perfil. ID Perfil $perfil");
                            }
                 //MisFunciones::auditoria("Usuarios Nuevo: ",Input::get('name'));
                // redirect
			Session::flash('message', $msg);
			return Redirect::to($this->textos[1]);
            } 
            
        }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
