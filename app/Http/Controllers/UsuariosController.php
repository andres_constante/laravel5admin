<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Hay que agregar las siguientes referencias
use DB;
use \Illuminate\Support\Facades;
//use \App;
use MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class UsuariosController extends Controller
{
       //Variables Principales
    var $textos=array("Usuarios del Sistema","usuarios","crear");
        var $camposcaption= array("Cédula","Nombres","Teléfono","Email","Fecha Nacimiento","Tipo de Usuario","Perfil");
        var $camposfield= array("cedula","name","telefono","email","fecha_nacimiento","id_tipo_usuario","id_perfil");
        var $campostype=array("text","text","text","text","date","select","select");
            //Para chosen// Clase y multiple
        var $multiple=array("","","","","","","");
        var $clase=array("","","","","","chosen-select","chosen-select");
            //                        
        var $lista=array(array(),array(),array(),array(),array(),array(),array());
        var $fecha= array("","","","","","","");
//
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->camposfield[6]="perfil";
        $tabla = DB::table("users as a")
                    ->join("ad_tipousuario as c","a.id_tipo_usuario","=","c.id")
                    ->select("a.*","c.tipo_usuario",
                            DB::raw("(select perfil from ad_perfil where id=a.id_perfil) as perfil"))->get();
            /*echo "<pre>";
            print_r($tabla);die();
            die();*/            
            $this->camposfield[5]="tipo_usuario";
            return view('index.mainindex',["tabla"=>$tabla,"textos"=>$this->textos,
                "camposcaption"=>$this->camposcaption,"camposfield"=>$this->camposfield]);            
    }
    public function guardar($id)
    {            
            $ruta=$this->textos[1];
            $msg="Registro Creado Exitosamente...!";
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new \App\UsuariosModel();
                $this->camposfield[]="password";
                $msgauditoria="Registro de Nuevo Usuario";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= \App\UsuariosModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Actualizar datos Usuario";
            }
            $input=Input::all();
            $validator = Validator::make($input, \App\UsuariosModel::rules($id));
            /*echo "<pre>";
            print_r($input);die();*/
            //die($ruta);
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                //die("Hola");
                 foreach($this->camposfield as $keycam => $valuecam)
                 {
                     if($valuecam=='password')
                     {
                        $guardar->password=bcrypt('123456'); 
                     }else{
                        $cadena="\$guardar->".$valuecam." = Input::get('$valuecam');";
                        eval($cadena);                                   
                     }
                 }                                               
                 $guardar->save();
                  /*Permisos por Perfil*/
                        $perfil= Input::get('id_perfil');
                        if($perfil)
                        {
                            $usuario=$guardar->id;
                            $permiper= \App\PerfilPermisosModel::where("idperfil",$perfil)->get();
                            DB::table('ad_menuusuario')->where('idusuario', '=', $usuario)->delete();
                            foreach ($permiper as $key => $value) {
                                $permisos= new \App\PermisosUsuariosModel();
                                $permisos->idusuario  = $usuario;
                                $permisos->idmenu     = $value->idmenu;
                                $permisos->save();
                            }
                        }
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($this->camposfield[0]));
                // redirect
			Session::flash('message', $msg);
			return Redirect::to($this->textos[1]);
            } 
            
        }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //        
            $listatc= \App\TipousuarioModel::lists('tipo_usuario','id')->all();
            $listatd= \App\PerfilModel::lists("perfil","id")->all();
            $this->lista=array(array(),array(),array(),array(),array(),$listatc,$listatd);
            /**/
            $this->fecha[4]= fechas(1);
            return view('index.maincreate',[
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype,
                    "lista"=>$this->lista,
                    "multiple"=>$this->multiple,
                    "clase"=>$this->clase,
                    "fecha"=>$this->fecha
                    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //        
        $this->camposfield[6]="perfil";
        $this->camposfield[5]="tipo_usuario";
        $tabla = DB::table("users as a")
                ->join("ad_tipousuario as c","a.id_tipo_usuario","=","c.id")
                ->select("a.*","c.tipo_usuario",
                DB::raw("(select perfil from ad_perfil where id=a.id_perfil) as perfil"))
                ->where("a.id",$id)
                ->first();
            /*  echo "<pre>";
            print_r($tabla);die();*/
            $titucampo=$this->textos[0].":";            
            return \View::make('index.mainshow')
                    ->with("tabla",$tabla)
                ->with("textos",$this->textos)
                ->with("camposcaption",$this->camposcaption)
                ->with("camposfield",$this->camposfield)                
                ->with("titucampo",$titucampo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $listatc= \App\TipousuarioModel::lists('tipo_usuario','id')->all();
        $listatd= \App\PerfilModel::lists("perfil","id")->all();
        $this->lista=array(array(),array(),array(),array(),array(),$listatc,$listatd);
        /**/
        $this->fecha[4]= fechas(1);
        $tabla = \App\UsuariosModel::find($id);
        $this->textos[2]="editar";
        return view('index.maincreate',[
                    "tabla"=>$tabla,
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype,
                    "lista"=>$this->lista,
                    "multiple"=>$this->multiple,
                    "clase"=>$this->clase,
                    "fecha"=>$this->fecha
                    ]);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
