<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Hay que agregar las siguientes referencias
use DB;
use \Illuminate\Support\Facades;
//use \App;
use MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class PermisosUsuariosController extends Controller
{
    var $textos=array("Permisos de Usuarios","permisosusuario","crear","permisosasignar");
        var $camposcaption= array("Usuario");
        var $camposfield= array("idusuario");
        var $campostype=array("select");
            //Para chosen// Clase y multiple
        var $multiple=array('');
        var $clase=array('');
            //                        
        var $lista=array(array());
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //        
            $usuario= \App\UsuariosModel::where("email","<>","admin@admin.com")->lists("name","id")->all();
            $this->lista[0]=$usuario;
            /*echo "<pre>";
            print_r($usuario);
            die();*/
            return view('indextree.mainindex',[
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype,
                    "lista"=>$this->lista,
                    "multiple"=>$this->multiple,
                    "clase"=>$this->clase
                    ]);            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tabla= \App\PermisosUsuariosModel::all(); 
            $usuario= \App\UsuariosModel::where("email","<>","admin@admin.com")->lists("name","id")->all();
            $this->lista[0]=$usuario;

            return view('indextree.maincreate',[
                    "tabla"=>$tabla,
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype,
                    "lista"=>$this->lista,
                    "multiple"=>$this->multiple,
                    "clase"=>$this->clase
                    ]); 
    }
        public function guardar($id)
        {            
            $ruta=$this->textos[1];
            $msg="Permisos asignados Exitosamente...!";
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new \App\PermisosUsuariosModel;
            }
            else{
                $ruta.="/$id/edit";
                $guardar= \App\PermisosUsuariosModel::find($id);
                $msg="Permisos Actualizados Exitosamente...!";
            }
            $input=Input::all();
            $validator = Validator::make($input, \App\PermisosUsuariosModel::rules($id));
            /*echo "<pre>";
            print_r($input);die();*/
            //die($ruta);
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                //die($ruta);
                        $sw=0;
			$permimenu = Input::get('idmenumain');
                        $usuario=Input::get('idusuario');
                        /*echo "<pre>";
                        echo "Usuario: $usuario <br>";
                        print_r($permimenu);
                        die();*/
                        DB::table('ad_menuusuario')->where('idusuario', '=', $usuario)->delete();
                        //print_r($permimenu);
//                        if(is_array($permimenu))
  //                      {
                            foreach ($permimenu as $valor) {
                                echo "Menu:  $valor <br>";
                                $permisos = new \App\PermisosUsuariosModel;
                                $permisos->idusuario  = $usuario;
                                $permisos->idmenu     = $valor;
                                $verpa= DB::table('ad_menuusuario')->where(function($query)use($valor,$usuario){
                                    $query->where("idusuario",$usuario)->where("idmenu",$valor);
                                })->first();
                                if(!$verpa)
                                {
                                    $permisos->save();
                                }
                                $sw=1;
                                /*Ver padre del hijo*/
                                $verpa= DB::table('menu')->where("id",$valor)->first();
                                $idpa=$verpa->idmain;
                                $verpa= DB::table('ad_menuusuario')->where(function($query)use($idpa,$usuario){
                                    $query->where("idusuario",$usuario)->where("idmenu",$idpa);
                                })->first();
                                if(!$verpa)
                                {
                                    $permisos = new \App\PermisosUsuariosModel;
                                    $permisos->idusuario  = $usuario;
                                    $permisos->idmenu     = $idpa;
                                    
                                    $permisos->save();
                                }
                                Auditoria("Asignación de Permisos a usuario. ID Usuario $usuario");
                            }
                 //MisFunciones::auditoria("Usuarios Nuevo: ",Input::get('name'));
                // redirect
			Session::flash('message', $msg);
			return Redirect::to($this->textos[1]);
            } 
            
        }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
