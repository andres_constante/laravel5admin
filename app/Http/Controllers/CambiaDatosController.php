<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Hay que agregar las siguientes referencias
use DB;
use \Illuminate\Support\Facades;
//use \App;
use MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class CambiaDatosController extends Controller
{
       //Variables Principales
    //$campos = array('nombres','cedula','email','telefono','fecha_nacimiento','password','password_confirmation');
    //$detalle = array('Nombres','Cédula','Email','Teléfono','Fecha de Nacimiento','Contraseña','Confirmar Contraseña');
    var $textos=array("Datos del Usuario","cambiardatos","editar");
        var $camposcaption= array("Cédula","Nombres","Teléfono","Email","Fecha Nacimiento","Contraseña","Confirmar Contraseña");
        var $camposfield= array("cedula","name","telefono","email","fecha_nacimiento","password","password_confirmation");
        var $campostype=array("text","text","text","text","date","password","password");
            //Para chosen// Clase y multiple
        var $multiple=array("","","","","","","");
        var $clase=array("","","","","","","");
            //                        
        var $lista=array(array(),array(),array(),array(),array(),array(),array());
        var $fecha= array("","","","","","","");
//
    public function __construct() {
        $this->middleware('auth');
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**/ 
        /*unset($this->camposcaption[5]);        
        unset($this->camposcaption[6]);
        unset($this->camposfield[5]);
        unset($this->camposfield[6]);
        array_splice($this->camposcaption,5,0);
        array_splice($this->camposcaption,6,0);
        array_splice($this->camposfield,5,0);
        array_splice($this->camposfield,5,0);
         * 
         */
        $tabla = \App\UsuariosModel::find($id);
        if(!$tabla)                
                $this->fecha[4]= fechas(1);
            else
                $this->fecha[4]=$tabla->fecha_nacimiento;        
        return view('index.maincreate',[
                    "tabla"=>$tabla,
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype,
                    "lista"=>$this->lista,
                    "multiple"=>$this->multiple,
                    "clase"=>$this->clase,
                    "fecha"=>$this->fecha,
                    "nobloqueo"=>0
                    ]);           
    }
    public function guardar($id)
    {            
        unset($this->camposcaption[6]);
        unset($this->camposfield[6]); 
        array_splice($this->camposcaption,6,0);
        array_splice($this->camposfield,6,0);
        
            $ruta=$this->textos[1]."/$id/edit";            
            $guardar= \App\UsuariosModel::find($id);
            $msg="Datos Actualizados Exitosamente...!";            
            $input=Input::all();
            $validator = Validator::make($input, \App\UsuariosModel::rulescon($id));
            /*echo "<pre>";
            print_r($validator);die();*/            
            if ($validator->fails()) {
                return Redirect::to($ruta)
                    ->withErrors($validator)
                    ->withInput(Input::except('password'));
            }else {
                //die("Hola");
                 foreach($this->camposfield as $keycam => $valuecam)
                 {
                     if($valuecam=='password' && !$id)
                     {
                        $guardar->password=bcrypt('123456'); 
                     }elseif($valuecam=='password'){
                         $guardar->password=bcrypt(Input::get('password')); 
                     }else{
                        $cadena="\$guardar->".$valuecam." = Input::get('$valuecam');";
                        eval($cadena);                                   
                     }
                 }
                 $guardar->nuevo=0;
                 $guardar->save();
                 Auditoria("Actualización de Datos de Usuario");
                // redirect
                Session::flash('message', $msg);
                return Redirect::to("/");
            } 
            
        }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
    }
}
