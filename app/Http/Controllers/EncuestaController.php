<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Hay que agregar las siguientes referencias
use DB;
use \Illuminate\Support\Facades;
//use \App;
use MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class EncuestaController extends Controller
{
    var $textos=array("Encuesta a Recicladores","encuesta","crear");
        var $camposcaption= array("Cédula","Apellidos y Nombres","Edad","Lugar de Residencia","Fecha","Organización/Independiente",
                "Recicla UD. en el relleno sanitario del área","Tipo Reciclador","Reciclador Activo","Carnet GAD Manta","Producto Objetivo",
                "Volúmen","Horas Actividad","Precio","Vacunado contra el Tétano","Dosis","Hace que tiempo","Otras Vacunas");
        var $camposfield= array("cedula","nombres","edad","residencia","fecha","organización",
                "tiporeciclaje","tiporeciclador","recicladoractivo","carnetreciclador","productoobj",
                "volumen","horas","precio","vacunatetano","dosis","tiempo","otrasvacunas");
        var $campostype=array("text","text","text","text","date","text",
                "select","select","select","select","select",
                "select","text","text","select","text","text","text");
            //Para chosen// Clase y multiple
        var $multiple=array("","","","","","",
                "multiple","","","","multiple",
                "multiple","","","","","","");
        var $clase=array("","","","","","",
                "chosen-select","","","","chosen-select",
                "chosen-select","","","","","","");
            //                        
        var $lista=array();
        var $fecha =array("","","","","");
    public function __construct() {
        $this->middleware('auth');
    }   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla= \App\EncuestaModel::orderby("cedula")->get();
        return view('index.mainindex',["tabla"=>$tabla,"textos"=>$this->textos,
                "camposcaption"=>$this->camposcaption,"camposfield"=>$this->camposfield]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $valsel= array();
            foreach ($this->camposfield as $key => $value) {
                $tbconfig= \App\SysConfigModel::where("campo",$value)->first();
                if($tbconfig)
                {
//                    echo $tbconfig->valor."<br>";
                    $vector=  explode("|", $tbconfig->valor);
                    $cade="";
                    foreach ($vector as $key1 => $value1) {
                        $cade.="'$value1'=>'$value1',";
                    }
                    $cade = substr($cade, 0, -1);
                    eval("\$valsel[]= array($cade);");
                }
            }
            /*echo "<pre>";
            print_r($valsel);
            die();*/
        $this->lista=array(array(),array(),array(),array(),array(),array(),
                $valsel[0],$valsel[1],$valsel[2],$valsel[3],$valsel[4],
                $valsel[5],array(),array(),$valsel[6]);
        $this->fecha[4]=  fechas(1);
        return view('index.maincreate',[
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype,
                    "lista"=>$this->lista,
                    "multiple"=>$this->multiple,
                    "clase"=>$this->clase,
                    "fecha"=>$this->fecha   
                    ]); 
    }
    public function guardar($id)
    {            
            $ruta=$this->textos[1];
            $msg="Registro Creado Exitosamente...!";
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new \App\EncuestaModel;
                $msgauditoria="Registro de Reciclador";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= \App\EncuestaModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Actualizar Reciclador";
            }
            $input=Input::all();
            $validator = Validator::make($input, \App\EncuestaModel::rules($id));
            /*echo "<pre>";
            print_r($input);die();*/
            //die($ruta);
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                if($id==0)
                {
                    $vali=  \App\EncuestaModel::where(function($query){
                        $query->where("cedula",Input::get("cedula"))->where("fecha",fechas(1));
                    })->first();
                    if($vali)
                    {
                        return Redirect::to("$ruta")
                        ->withErrors(["La cédula ".Input::get("cedula"). " ya fue registrada el día de hoy ".fechas(2)])
                        ->withInput();
                    }
                }
                //die("Hola");
                 foreach($this->camposfield as $keycam => $valuecam)
                 {                     
                        $tbconfig= \App\SysConfigModel::where("campo",$valuecam)->first();
                        if($tbconfig)
                        {    
                            $multi=  $tbconfig->valorsub;
                            if($multi=="multiple")
                            {
                                $opciones = Input::get($valuecam);                                
                                $selec="";
                                foreach ($opciones as $key1) {
                                    $selec.=$key1."/";
                                }
                                $selec = substr($selec, 0, -1);                                
                                $cadena="\$guardar->".$valuecam." = '".$selec."';";                                
                                eval($cadena);
                            }else{
                                $cadena="\$guardar->".$valuecam." = Input::get('$valuecam');";
                                eval($cadena);
                            }
                        }else{
                            $cadena="\$guardar->".$valuecam." = Input::get('$valuecam');";
                            eval($cadena);
                        }
                 } 
                 //die("Listo");
                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($this->camposfield[0]));
                 //MisFunciones::auditoria("Usuarios Nuevo: ",Input::get('name'));
                // redirect
			Session::flash('message', $msg);
			return Redirect::to($this->textos[1]);
            } 
            
        }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tabla = \App\EncuestaModel::find($id);
            /*  echo "<pre>";
            print_r($tabla);die();*/
            $titucampo=$this->textos[0].":";            
            return \View::make('index.mainshow')
                    ->with("tabla",$tabla)
                ->with("textos",$this->textos)
                ->with("camposcaption",$this->camposcaption)
                ->with("camposfield",$this->camposfield)                
                ->with("titucampo",$titucampo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tabla= \App\EncuestaModel::find($id);
        $valsel= array();
        $selec= array();
            foreach ($this->camposfield as $key => $value) {
                $tbconfig= \App\SysConfigModel::where("campo",$value)->first();
                if($tbconfig)
                {
//                    echo $tbconfig->valor."<br>";
                    $vector=  explode("|", $tbconfig->valor);
                    $cade="";
                    foreach ($vector as $key1 => $value1) {
                        $cade.="'$value1'=>'$value1',";
                    }
                    $cade = substr($cade, 0, -1);
                    eval("\$valsel[]= array($cade);");
                    //Para Valores Seleccionados
                    if($tbconfig->valorsub=="multiple")
                    {
                        $cadexe="\$valork=\$tabla->".$value.";";                        
                        eval($cadexe);                        
                        $valork=str_replace("/","','",$valork);
                        $cadexe="\$select[]=array('$valork');";
                        //echo $cadexe;
                        eval($cadexe);
                    }else {
                        $select[]=array();
                    }
                }else{
                    $select[]=array();
                }
                
            }
            /*echo "<pre>";
            print_r($select);
            die();*/
        $this->lista=array(array(),array(),array(),array(),array(),array(),
                $valsel[0],$valsel[1],$valsel[2],$valsel[3],$valsel[4],
                $valsel[5],array(),array(),$valsel[6]);
        $this->fecha[4]= $tabla->fecha;//  fechas(1);
        $this->textos[2]="editar";        
        
        return view('index.maincreate',[
                    "tabla"=>$tabla,
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype,
                    "lista"=>$this->lista,
                    "multiple"=>$this->multiple,
                    "clase"=>$this->clase,
                    "fecha"=>$this->fecha ,
                    "select"=>$select
                    ]); //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar(1);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
