<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Hay que agregar las siguientes referencias
use DB;
use \Illuminate\Support\Facades;
//use \App;
use MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class TipousuarioController extends Controller
{
        var $textos=array("Tipos de Usuario","tipousuario","crear");
        var $camposcaption= array("Tipo");
        var $camposfield= array("tipo_usuario");
        var $campostype=array("text");
            //Para chosen// Clase y multiple
        var $multiple=array("");
        var $clase=array("");
            //                        
        var $lista=array(array());
    public function __construct() {
        $this->middleware('auth');
    }        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla=  \App\TipoUsuarioModel::all();
        return view('index.mainindex',["tabla"=>$tabla,"textos"=>$this->textos,
                "camposcaption"=>$this->camposcaption,"camposfield"=>$this->camposfield]); 
    }
    public function guardar($id)
    {            
            $ruta=$this->textos[1];
            $msg="Registro Creado Exitosamente...!";
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new \App\TipoUsuarioModel;  
                $msgauditoria="Registro de Tipo de Usuario";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= \App\TipoUsuarioModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Actualizar Tipo de Usuario";
            }
            $input=Input::all();
            $validator = Validator::make($input, \App\TipoUsuarioModel::rules($id));
            /*echo "<pre>";
            print_r($input);die();*/
            //die($ruta);
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                //die("Hola");
                 foreach($this->camposfield as $keycam => $valuecam)
                 {
                     if($valuecam=='password')
                     {
                        $guardar->password=bcrypt('123456'); 
                     }else{
                        $cadena="\$guardar->".$valuecam." = Input::get('$valuecam');";
                        eval($cadena);                                   
                     }
                 }                                               
                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($this->camposfield[0]));
                 //MisFunciones::auditoria("Usuarios Nuevo: ",Input::get('name'));
                // redirect
			Session::flash('message', $msg);
			return Redirect::to($this->textos[1]);
            } 
            
        }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('index.maincreate',[
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype
                    ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tabla=  \App\TipoUsuarioModel::where("id",$id)->first();
        $titucampo=$this->textos[0].":";
        return \View::make('index.mainshow')
                    ->with("tabla",$tabla)
                ->with("textos",$this->textos)
                ->with("camposcaption",$this->camposcaption)
                ->with("camposfield",$this->camposfield)                
                ->with("titucampo",$titucampo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->textos[2]="editar";
        $tabla=  \App\TipoUsuarioModel::find($id);
        return view('index.maincreate',[
                    "tabla"=>$tabla,        
                    "textos"=>$this->textos,
                    "camposcaption"=>$this->camposcaption,
                    "camposfield"=>$this->camposfield,
                    "campostype"=>$this->campostype
                    ]);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
