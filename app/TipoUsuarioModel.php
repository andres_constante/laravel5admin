<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUsuarioModel extends Model
{
    //
    protected $table = 'ad_tipousuario';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'tipo_usuario'=>'required|unique:ad_tipousuario'. ($id ? ",id,$id" : ''),
            ], $merge);
        } 
    
}
