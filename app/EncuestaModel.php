<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EncuestaModel extends Model
{
    //
    protected $table = 'encuesta';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                /*'campo'=>'required|unique:sysconfig'. ($id ? ",id,$id" : ''),
                'valor'=>'required',*/
                "cedula"=>"required",            
            "nombres"=>"required",
            "edad"=>"required|numeric",
            "residencia"=>"required",
            "fecha"=>"required|date",
            "organización"=>"required",
            "tiporeciclaje"=>"required",
            "tiporeciclador"=>"required",
            "recicladoractivo"=>"required",
            "carnetreciclador"=>"required",
            "productoobj"=>"required",
            "volumen"=>"required",
            "horas"=>"required",
            "precio"=>"required",
            "vacunatetano"=>"required",
            "dosis"=>"required|numeric",
            "tiempo"=>"required"
            ], $merge);
        }  
}
