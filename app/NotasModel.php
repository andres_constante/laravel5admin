<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotasModel extends Model
{
    //
    protected $table = 'notas';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'nombres'=>'required|unique:notas'. ($id ? ",id,$id" : ''),
                'nota1'=>'numeric|between:0,10',
                'nota2'=>'numeric|between:0,10',
                'nota3'=>'numeric|between:0,10',
                'nota4'=>'numeric|between:0,10',
                'suma'=>'numeric',
                'promedio'=>'numeric'
            ], $merge);
        }    
}
