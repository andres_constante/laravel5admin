<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysConfigModel extends Model
{
    //
    protected $table = 'sysconfig';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'campo'=>'required|unique:sysconfig'. ($id ? ",id,$id" : ''),
                'valor'=>'required'
            ], $merge);
        }  
}
