-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.6.17


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema laravel5admin
--

CREATE DATABASE IF NOT EXISTS laravel5admin;
USE laravel5admin;

--
-- Definition of table `ad_auditoria`
--

DROP TABLE IF EXISTS `ad_auditoria`;
CREATE TABLE `ad_auditoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) NOT NULL,
  `urlmenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `accion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nompc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_auditoria`
--

/*!40000 ALTER TABLE `ad_auditoria` DISABLE KEYS */;
INSERT INTO `ad_auditoria` (`id`,`idusuario`,`urlmenu`,`accion`,`ip`,`nompc`,`created_at`,`updated_at`) VALUES 
 (1,3,'/laravel5admin/public/usuarios','Acceso no permitido 403','::1','localhost','2016-05-23 14:58:53','2016-05-23 14:58:53'),
 (2,3,'/laravel5admin/public/cambiardatos/3/edit','Acceso no permitido 403','::1','localhost','2016-05-23 15:08:49','2016-05-23 15:08:49'),
 (3,3,'/laravel5admin/public/cambiardatos/3','Actualización de Datos de Usuario','::1','localhost','2016-05-23 15:11:56','2016-05-23 15:11:56'),
 (4,3,'/laravel5admin/public/cambiardatos/3','Actualización de Datos de Usuario','::1','localhost','2016-05-23 15:13:49','2016-05-23 15:13:49'),
 (5,1,'/laravel5admin/public/menu/3','Actualizar Menú de Opciones - ID: ','::1','localhost','2016-05-23 16:17:02','2016-05-23 16:17:02'),
 (6,1,'/laravel5admin/public/menu/3','Actualizar Menú de Opciones - ID: 3-Auditoría','::1','localhost','2016-05-23 16:19:53','2016-05-23 16:19:53'),
 (7,1,'/laravel5admin/public/usuarios/3','Actualizar datos Usuario - ID: 3-1309366514','::1','localhost','2016-05-23 16:26:11','2016-05-23 16:26:11'),
 (8,1,'/laravel5admin/public/perfilpermisos','Asignación de Permisos a Perfil. ID Perfil 1','::1','localhost','2016-05-23 16:30:29','2016-05-23 16:30:29'),
 (9,1,'/laravel5admin/public/config','Registro de Perfil de Usuario - ID: 0-Tipo_Reciclaje','::1','localhost','2016-05-23 17:09:03','2016-05-23 17:09:03'),
 (10,1,'/laravel5admin/public/config/1','Actualizar Perfil de Usuario - ID: 1-Tipo_Reciclaje','::1','localhost','2016-05-23 17:09:15','2016-05-23 17:09:15'),
 (11,1,'/laravel5admin/public/config/1','Actualizar Perfil de Usuario - ID: 1-tiporeciclaje','::1','localhost','2016-05-23 17:09:31','2016-05-23 17:09:31'),
 (12,1,'/laravel5admin/public/config','Registro de Perfil de Usuario - ID: 0-tiporeciclador','::1','localhost','2016-05-23 17:09:56','2016-05-23 17:09:56'),
 (13,1,'/laravel5admin/public/config','Registro de Perfil de Usuario - ID: 0-recicladoractivo','::1','localhost','2016-05-23 17:10:08','2016-05-23 17:10:08'),
 (14,1,'/laravel5admin/public/config','Registro de Perfil de Usuario - ID: 0-carnetreciclador','::1','localhost','2016-05-23 17:10:30','2016-05-23 17:10:30'),
 (15,1,'/laravel5admin/public/config','Registro de Perfil de Usuario - ID: 0-productoobj','::1','localhost','2016-05-23 17:12:07','2016-05-23 17:12:07'),
 (16,1,'/laravel5admin/public/config','Registro de Perfil de Usuario - ID: 0-volumen','::1','localhost','2016-05-23 17:12:33','2016-05-23 17:12:33'),
 (17,1,'/laravel5admin/public/config','Registro de Perfil de Usuario - ID: 0-vacunatetano','::1','localhost','2016-05-23 17:12:54','2016-05-23 17:12:54'),
 (18,1,'/laravel5admin/public/menu','Registro de Menú de Opciones - ID: 0-Variables de COnfiguración','::1','localhost','2016-05-23 17:14:31','2016-05-23 17:14:31'),
 (19,1,'/laravel5admin/public/menu','Registro de Menú de Opciones - ID: 0-Maestros','::1','localhost','2016-05-23 17:16:06','2016-05-23 17:16:06'),
 (20,1,'/laravel5admin/public/menu/12','Actualizar Menú de Opciones - ID: 12-Maestros','::1','localhost','2016-05-23 17:16:14','2016-05-23 17:16:14'),
 (21,1,'/laravel5admin/public/menu','Registro de Menú de Opciones - ID: 0-Encuesta','::1','localhost','2016-05-23 17:16:34','2016-05-23 17:16:34'),
 (22,1,'/laravel5admin/public/config/6','Actualizar Perfil de Usuario - ID: 6-volumen','::1','localhost','2016-05-23 23:23:55','2016-05-23 23:23:55'),
 (23,1,'/laravel5admin/public/config/5','Actualizar Perfil de Usuario - ID: 5-productoobj','::1','localhost','2016-05-23 23:24:03','2016-05-23 23:24:03'),
 (24,1,'/laravel5admin/public/encuesta','Registro de Reciclador - ID: 0-1309366514','::1','localhost','2016-05-23 23:50:06','2016-05-23 23:50:06'),
 (25,1,'/laravel5admin/public/encuesta','Registro de Reciclador - ID: 0-1309366514','::1','localhost','2016-05-23 23:53:52','2016-05-23 23:53:52'),
 (26,1,'/laravel5admin/public/encuesta','Registro de Reciclador - ID: 0-1309366514','::1','localhost','2016-05-24 00:36:06','2016-05-24 00:36:06'),
 (27,1,'/laravel5admin/public/encuesta/1','Actualizar Reciclador - ID: 1-1309366514','::1','localhost','2016-05-24 00:37:50','2016-05-24 00:37:50'),
 (28,1,'/laravel5admin/public/encuesta/1','Actualizar Reciclador - ID: 1-1309366514','::1','localhost','2016-05-24 00:39:11','2016-05-24 00:39:11'),
 (29,1,'/laravel5admin/public/config/1','Actualizar Perfil de Usuario - ID: 1-tiporeciclaje','::1','localhost','2016-05-24 09:22:00','2016-05-24 09:22:00'),
 (30,1,'/laravel5admin/public/encuesta','Registro de Reciclador - ID: 0-1309366514','::1','localhost','2016-05-24 09:24:41','2016-05-24 09:24:41'),
 (31,1,'/laravel5admin/public/encuesta/1','Actualizar Reciclador - ID: 1-1309366514','::1','localhost','2016-05-24 09:26:02','2016-05-24 09:26:02'),
 (32,1,'/laravel5admin/public/encuesta/1','Actualizar Reciclador - ID: 1-1309366514','::1','localhost','2016-05-24 09:26:17','2016-05-24 09:26:17'),
 (33,1,'/laravel5admin/public/encuesta','Registro de Reciclador - ID: 0-1314494905','::1','localhost','2016-05-24 09:27:33','2016-05-24 09:27:33'),
 (34,1,'/laravel5admin/public/menu/12','Actualizar Menú de Opciones - ID: 12-Ingresos','::1','localhost','2016-05-24 09:33:26','2016-05-24 09:33:26');
/*!40000 ALTER TABLE `ad_auditoria` ENABLE KEYS */;


--
-- Definition of table `ad_menuusuario`
--

DROP TABLE IF EXISTS `ad_menuusuario`;
CREATE TABLE `ad_menuusuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) NOT NULL,
  `idmenu` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_menuusuario`
--

/*!40000 ALTER TABLE `ad_menuusuario` DISABLE KEYS */;
INSERT INTO `ad_menuusuario` (`id`,`idusuario`,`idmenu`,`created_at`,`updated_at`) VALUES 
 (25,3,2,NULL,NULL),
 (26,3,1,NULL,NULL);
/*!40000 ALTER TABLE `ad_menuusuario` ENABLE KEYS */;


--
-- Definition of table `ad_perfil`
--

DROP TABLE IF EXISTS `ad_perfil`;
CREATE TABLE `ad_perfil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `perfil` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACT',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_perfil`
--

/*!40000 ALTER TABLE `ad_perfil` DISABLE KEYS */;
INSERT INTO `ad_perfil` (`id`,`perfil`,`estado`,`created_at`,`updated_at`) VALUES 
 (1,'GENERAL','ACT',NULL,NULL),
 (2,'CONSULTAS','ACT','2016-05-23 07:10:35','2016-05-23 07:11:51'),
 (3,'INVITADOS','ACT','2016-05-23 07:11:05','2016-05-23 07:11:05');
/*!40000 ALTER TABLE `ad_perfil` ENABLE KEYS */;


--
-- Definition of table `ad_perfilpermisos`
--

DROP TABLE IF EXISTS `ad_perfilpermisos`;
CREATE TABLE `ad_perfilpermisos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idperfil` int(11) NOT NULL,
  `idmenu` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_perfilpermisos`
--

/*!40000 ALTER TABLE `ad_perfilpermisos` DISABLE KEYS */;
INSERT INTO `ad_perfilpermisos` (`id`,`idperfil`,`idmenu`,`created_at`,`updated_at`) VALUES 
 (48,1,2,'2016-05-23 16:30:29','2016-05-23 16:30:29'),
 (49,1,1,'2016-05-23 16:30:29','2016-05-23 16:30:29');
/*!40000 ALTER TABLE `ad_perfilpermisos` ENABLE KEYS */;


--
-- Definition of table `ad_tipousuario`
--

DROP TABLE IF EXISTS `ad_tipousuario`;
CREATE TABLE `ad_tipousuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_tipousuario_tipo_usuario_unique` (`tipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_tipousuario`
--

/*!40000 ALTER TABLE `ad_tipousuario` DISABLE KEYS */;
INSERT INTO `ad_tipousuario` (`id`,`tipo_usuario`,`created_at`,`updated_at`) VALUES 
 (1,'ADMINISTRADOR',NULL,NULL),
 (2,'USUARIO','2016-05-23 07:02:19','2016-05-23 07:04:10');
/*!40000 ALTER TABLE `ad_tipousuario` ENABLE KEYS */;


--
-- Definition of table `encuesta`
--

DROP TABLE IF EXISTS `encuesta`;
CREATE TABLE `encuesta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cedula` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `residencia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `organización` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tiporeciclaje` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tiporeciclador` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recicladoractivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `carnetreciclador` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `productoobj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `volumen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `horas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `precio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vacunatetano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dosis` int(11) NOT NULL,
  `tiempo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `otrasvacunas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `encuesta`
--

/*!40000 ALTER TABLE `encuesta` DISABLE KEYS */;
INSERT INTO `encuesta` (`id`,`cedula`,`nombres`,`edad`,`residencia`,`fecha`,`organización`,`tiporeciclaje`,`tiporeciclador`,`recicladoractivo`,`carnetreciclador`,`productoobj`,`volumen`,`horas`,`precio`,`vacunatetano`,`dosis`,`tiempo`,`otrasvacunas`,`created_at`,`updated_at`) VALUES 
 (1,'1309366514','Andrés Constante',31,'Manta','2016-05-24','Independiente','ESCOMBRERAS/DESECHOS COMUNES','PERMANENTE','SI','SI','COBRE/MADERA/LATA','LIBRAS','8','20','NO',2,'1 año','No','2016-05-24 09:24:41','2016-05-24 09:26:17'),
 (2,'1314494905','Virginia  Intriago',25,'Manta','2016-05-23','Indepndiente','ESCOMBRERAS','TEMPORAL','SI','SI','ALUMINIO/MONEL','LIBRAS','8','20','SI',2,'1 año','No','2016-05-24 09:27:33','2016-05-24 09:27:33');
/*!40000 ALTER TABLE `encuesta` ENABLE KEYS */;


--
-- Definition of table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ruta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `iconfaw` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `nivel` int(11) NOT NULL,
  `idmain` int(11) NOT NULL,
  `orden` decimal(10,2) NOT NULL,
  `adicional` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`,`menu`,`ruta`,`iconfaw`,`nivel`,`idmain`,`orden`,`adicional`,`created_at`,`updated_at`) VALUES 
 (1,'Administración','#','fa fa-pencil-square-o',1,1,'1.00','','2016-05-23 12:04:20','2016-05-23 12:04:20'),
 (2,'Menú de Opciones','menu','',2,1,'2.00','','2016-05-23 12:05:02','2016-05-23 12:05:02'),
 (3,'Auditoría','auditoria','',2,1,'3.00','','2016-05-23 12:05:34','2016-05-23 16:17:02'),
 (4,'Usuarios del Sistema','#','fa fa-user',1,4,'4.00','','2016-05-23 12:06:10','2016-05-23 12:06:17'),
 (5,'Usuarios','usuarios','',2,4,'5.00','','2016-05-23 12:07:20','2016-05-23 12:07:20'),
 (6,'Tipos de Usuarios','tipousuario','',2,4,'6.00','','2016-05-23 12:07:43','2016-05-23 12:08:21'),
 (7,'Permisos de Usuarios','permisosusuario','',2,4,'7.00','','2016-05-23 12:08:46','2016-05-23 12:08:46'),
 (8,'Perfiles de Usuario','#','fa fa-user-secret',1,8,'8.00','','2016-05-23 12:09:43','2016-05-23 12:09:52'),
 (9,'Perfiles','perfil','',2,8,'9.00','','2016-05-23 12:10:16','2016-05-23 12:10:16'),
 (10,'Permiso por Perfiles','perfilpermisos','',2,8,'10.00','','2016-05-23 12:10:42','2016-05-23 12:20:45'),
 (11,'Variables de Configuración','config','',2,1,'4.10','','2016-05-23 17:14:31','2016-05-23 17:14:31'),
 (12,'Ingresos','#','fa fa-asterisk',1,12,'11.00','','2016-05-23 17:16:05','2016-05-24 09:33:26'),
 (13,'Encuesta','encuesta','',2,12,'12.00','','2016-05-23 17:16:34','2016-05-23 17:16:34');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;


--
-- Definition of table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`,`batch`) VALUES 
 ('2014_10_12_000000_create_users_table',1),
 ('2014_10_12_100000_create_password_resets_table',1),
 ('2016_05_12_115733_create_menu_table',2),
 ('2016_05_12_103607_create_sessions_table',3),
 ('2016_05_20_112747_add_campos_field_to_users_table',4),
 ('2016_05_20_113221_create_tipousuario_table',4),
 ('2016_05_20_113438_create_ad_auditoria_table',4),
 ('2016_05_20_113522_create_ad_perfiles_table',4),
 ('2016_05_20_113533_create_ad_perfilpermisos_table',4),
 ('2016_05_20_113852_create_ad_menuusuario_table',4),
 ('2016_05_23_165145_create_encuesta_table',5),
 ('2016_05_23_170025_create_sysconfig_table',6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


--
-- Definition of table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;


--
-- Definition of table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` (`id`,`user_id`,`ip_address`,`user_agent`,`payload`,`last_activity`) VALUES 
 ('51ab5066074525a52698f0b6d67cfce3c5d5fbc9',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36','YTo1OntzOjY6Il90b2tlbiI7czo0MDoidHFVMlNCb1BnQXcwRG9vWUZSRFkxaHFHcFJNM0tTMEZFM2JNRWd0YyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTM6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9sYXJhdmVsNWFkbWluL3B1YmxpYy9hdXRoL2xvZ2luIjt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjM6InVybCI7YToxOntzOjg6ImludGVuZGVkIjtzOjQyOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbGFyYXZlbDVhZG1pbi9wdWJsaWMiO31zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ2NDEyNzc2OTtzOjE6ImMiO2k6MTQ2NDEyNzcwNDtzOjE6ImwiO3M6MToiMCI7fX0=',1464127769);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;


--
-- Definition of table `sysconfig`
--

DROP TABLE IF EXISTS `sysconfig`;
CREATE TABLE `sysconfig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorsub` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sysconfig_campo_unique` (`campo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sysconfig`
--

/*!40000 ALTER TABLE `sysconfig` DISABLE KEYS */;
INSERT INTO `sysconfig` (`id`,`campo`,`valor`,`valorsub`,`created_at`,`updated_at`) VALUES 
 (1,'tiporeciclaje','ESCOMBRERAS|DESECHOS COMUNES','multiple','2016-05-23 17:09:03','2016-05-24 09:22:00'),
 (2,'tiporeciclador','TEMPORAL|PERMANENTE','','2016-05-23 17:09:56','2016-05-23 17:09:56'),
 (3,'recicladoractivo','SI|NO','','2016-05-23 17:10:08','2016-05-23 17:10:08'),
 (4,'carnetreciclador','SI|NO','','2016-05-23 17:10:30','2016-05-23 17:10:30'),
 (5,'productoobj','HIERRO|ALUMINIO|COBRE|MONEL|BRONCE|MADERA|LATA|VIDRIO|PAPEL|CARTÓN|PLÁSTICO|CHALINA|SOPLADO','multiple','2016-05-23 17:12:07','2016-05-23 23:24:03'),
 (6,'volumen','LIBRAS|KILOS','multiple','2016-05-23 17:12:33','2016-05-23 23:23:55'),
 (7,'vacunatetano','SI|NO','','2016-05-23 17:12:54','2016-05-23 17:12:54');
/*!40000 ALTER TABLE `sysconfig` ENABLE KEYS */;


--
-- Definition of table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cedula` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000000000',
  `fecha_nacimiento` date NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL DEFAULT '1',
  `id_perfil` int(11) DEFAULT '0',
  `nuevo` int(11) NOT NULL DEFAULT '1',
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACT',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_cedula_unique` (`cedula`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`cedula`,`telefono`,`fecha_nacimiento`,`id_tipo_usuario`,`id_perfil`,`nuevo`,`estado`) VALUES 
 (1,'Administrador','admin@admin.com','$2y$10$wWhm4D6/Qa.G6AouNG0EfuWCCkJQCHP1avM4bMJWUer87l8nJCsJ.','KWxJcQGXeWvd0tjkLfij6gjXBfQGe63zexs9Zz3aEJn8a09uJknXFfzNloP4',NULL,'2016-05-24 00:50:57','0000000000','0000000000','0000-00-00',1,0,1,'ACT'),
 (3,'Andres Constante','aconstante@manta.gob.ec','$2y$10$xoPe8kevW6dOS/kffod1GOXC2MX6x5GWQ9zyy6vWhoSVy8gXKe9Qm','F311puQw4zSwV5bHbkd2kS6eCG5nBR6RR1B02gH1e1sphpRflZXWZONXlFwG','2016-05-20 14:59:40','2016-05-23 16:14:33','1309366514','0994978134','2016-05-23',1,1,0,'ACT');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
