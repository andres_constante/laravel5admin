<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>@yield('titulo')</title>
   
    {!! HTML::style('css/bootstrap.min.css') !!}       
    {!! HTML::style('font-awesome/css/font-awesome.css') !!}
 
   <!-- Data Tables -->
    {{ HTML::style('css/plugins/dataTables/dataTables.bootstrap.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.responsive.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.tableTools.min.css') }}
       
<!-- Mensaje -->

{{ HTML::style('css/plugins/toastr/toastr.min.css') }}
    {{ HTML::style('css/animate.css') }}
    {{ HTML::style('css/style.css') }}

    @yield('estilos')
<!-- Mainly scripts -->
{{ HTML::script('js/jquery-2.1.1.js') }} 
{{ HTML::script('js/bootstrap.min.js') }} 
{{ HTML::script('js/plugins/metisMenu/jquery.metisMenu.js') }} 
{{ HTML::script('js/plugins/slimscroll/jquery.slimscroll.min.js') }} 


<!-- Custom and plugin javascript -->
{{ HTML::script('js/inspinia.js') }} 
{{ HTML::script('js/plugins/pace/pace.min.js') }} 

 <!-- Data Tables -->
 {{ HTML::script('js/plugins/dataTables/jquery.dataTables.js') }}     
    {{ HTML::script("js/plugins/dataTables/dataTables.bootstrap.js") }} 
    {{ HTML::script("js/plugins/dataTables/dataTables.responsive.js") }} 
    {{ HTML::script("js/plugins/dataTables/dataTables.tableTools.min.js") }} 

 <!-- Mensaje -->
 {{ HTML::script('js/plugins/toastr/toastr.min.js') }}     
 
      <!-- CSS Notificacion -->
    {{ HTML::style('ventanas-modales/ventanas-modales.css') }}
    <!-- CSS Notificacion -->
    {{ HTML::script('ventanas-modales/ventanas-modales.js') }}  
    <!-- Graficos estadissticos -->
{{ HTML::style('css/plugins/morris/morris-0.4.3.min.css') }}
{{ HTML::script('js/plugins/morris/raphael-2.1.0.min.js') }}
{{ HTML::script('js/plugins/morris/morris.js') }}
<!-- Chosen -->
    {{ HTML::style('chosen/docsupport/prism.css') }}
    {{ HTML::style('chosen/chosen.css') }}
    
    {{ HTML::script('chosen/chosen.jquery.js') }}
    {{ HTML::script('chosen/docsupport/prism.js') }}
<!-- ColorBox -->
    {{ HTML::style('colorbox/colorbox.css') }}    
    
    {{ HTML::script('colorbox/jquery.colorbox.js') }}    
@yield('scripts')
 <script>
 function cargaatender()
 {
	 
	    $.ajax({
			type: "GET",
			url: '{{ URL::to("getnotificatodos") }}',
			data: '',
			error: function(objeto, quepaso, otroobj){
				$("#txttodos").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
			},				
			success: function(datos){
				$("#txttodos").html(datos);
		  },
  			statusCode: {
			    404: function() {
				$("#txttodos").html("<div class='alert alert-danger'>No existe URL</div>");
		    }	
			}	  
    }); 	 
 }
 function NotificacionTotal()
 {
	    $.ajax({
			type: "GET",
			url: '{{ URL::to("getnotificacuenta") }}',
			data: '',
			error: function(objeto, quepaso, otroobj){
				$("#txtcontar").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
			},				
			success: function(datos){
				$("#txtcontar").html(datos);
		  },
  			statusCode: {
			    404: function() {
				$("#txtcontar").html("<div class='alert alert-danger'>No existe URL</div>");
		    }	
			}	  
    }); 
 	 //setTimeout(NotificacionTotal, 5000);
 }
 //setTimeout(NotificacionTotal, 1000);
 

 </script>
</head>

<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">
                                    {{ Session::get('nombreusuario') }}
                                        </strong>
                             </span> <span class="text-muted text-xs block">
<i class="fa fa-user"></i>
                                 <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="{{ URL::to('auth/logout') }}">Salir del Sistema</a></li>
                            </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li {{ (Request::is('/') ? 'class="active"' : '') }}>
                    <a href="{{ URL::to('/') }}"><i class="fa fa-home"></i> <span class="nav-label">Inicio</span></a>
                </li>    
            @if (Session::has('tipousuario') && Session::get('tipousuario')=="ADMINISTRADOR")
                <li {{ (Request::is('institucion*') ? 'class="active"' : (Request::is('horarios*') ? 'class="active"' : (Request::is('usuarios*') ? 'class="active"' : (Request::is('recuhorario*') ? 'class="active"' : ''))))  }}>
                <a href="#"><i class="fa fa-pencil-square-o"></i> <span class="nav-label">Administración</span> <span class="fa arrow"></span></a>
                           <ul class="nav nav-second-level">
                             <li {{ (Request::is('institucion*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('institucion') }}">Datos Institucionales</a>
                              </li>                                                              
                               <li {{ (Request::is('usuarios*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('usuarios') }}">Usuarios</a>
                              </li>
                        </ul>
                </li>                    
            @endif
            <li {{ (Request::is('marcavehi*') ? 'class="active"' : (Request::is('modelo*') ? 'class="active"' : (Request::is('tipovehi*') ? 'class="active"' : (Request::is('vehiculo*') ? 'class="active"' : (Request::is('chofer*') ? 'class="active"' : (Request::is('tipogas*') ? 'class="active"' : (Request::is('*tickettodos/*') ? 'class="active"' : ' '))))))) }}>
                <a href="#"><i class="fa fa-asterisk"></i> <span class="nav-label">Maestros</span> <span class="fa arrow"></span></a>
                           <ul class="nav nav-second-level">                              
                              <li {{ (Request::is('marcavehi*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('marcavehi') }}">Marcas de Vehículos</a>
                              </li>
                              <li {{ (Request::is('modelo*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('modelo') }}">Modelos de Vehículos</a>
                              </li>
                              <li {{ (Request::is('tipovehi*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('tipovehi') }}">Tipo de Vehículos</a>
                              </li>
                              <li {{ (Request::is('vehiculo*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('vehiculo') }}">Vehículos</a>
                              </li>
                              <li {{ (Request::is('chofer*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('chofer') }}">Choferes Vehículos</a>
                              </li>
                              <li {{ (Request::is('tipogas*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('tipogas') }}">Tipo Gasolina</a>
                              </li>
                              
                        </ul>
                    </li>  
                    <li {{ (Request::is('controlvehiculo*') ? 'class="active"' : (Request::is('controlgasolina*') ? 'class="active"' : '')) }}>
                <a href="#"><i class="fa fa-archive"></i> <span class="nav-label">Movimientos</span><span class="fa arrow"></span></a>
                           <ul class="nav nav-second-level">
                             <li {{ (Request::is('controlvehiculo*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('controlvehiculo') }}">Control Vehículos</a>
                              </li>
                              <li {{ (Request::is('controlgasolina*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('controlgasolina') }}">Control Gasolina</a>
                              </li>
                        </ul>
                    </li>                                        
                        <li {{ (Request::is('reportes*') ? 'class="active"' : '') }}>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Reportes </span><span class="fa arrow"></span></a>
                           <ul class="nav nav-second-level">
                             <li {{ (Request::is('marcaciones*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('marcaciones') }}">Reporte 1</a>
                              </li>
                              <li {{ (Request::is('vacaciones*') ? 'class="active"' : '') }}>
                                       <a href="{{ URL::to('vacaciones') }}">Reporte 2</a>
                              </li>
                        </ul>
                    </li>                    
                     <li {{ (Request::is('salir') ? 'class="active"' : '') }}>
                                <a href="{{ URL::to('auth/logout') }}">      <i class="fa fa-sign-out"></i>  Salir</a>
                              </li>
            </ul>

        </div>
    </nav>
<div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <!--form role="search" class="navbar-form-custom" method="post" action="#">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form-->
                </div>
                <ul class="nav navbar-top-links navbar-right">
					<!-- Alerta Campana -->					
					@if (Session::has('tipousuario') && Session::get('tipousuario')=="ADMINISTRADOR")
					<li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="javascript::" onclick="cargaatender()">
                        <i class="fa fa-bell"></i>  <span class="label label-primary" id="txtcontar">0</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts" id="txttodos">
					
                    </ul>
                </li>
				@endif				
                    <li>
                        @if(Auth::user()->id==1)
                        <a href="#">
                            <i class="fa fa-user"></i> {{ Auth::user()->name }}
                        </a>
                        @else
                        <a href="{{ URL::to('cambiardatos/'.Auth::user()->id.'/edit') }}">
                            <i class="fa fa-user"></i> {{ Auth::user()->name }}
                        </a>
                        @endif
                    </li>                     
                    <li>
                        <a href="{{ URL::to('auth/logout') }}">
                            <i class="fa fa-sign-out"></i> Salir
                        </a>
                    </li>
                   
                    
                </ul>

            </nav>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">                
                <div class="col-lg-12">
                        @yield('contenido')
                </div>

            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                <!--10GB of <strong>250GB</strong> Free. -->
                {{ Auth::user()->name }}
            </div>
            <div>               
                {{ trans('html.main.copyright') }}
            </div>
        </div>

    </div>
</div>

</body>

</html>
