@extends('auth')
@section('titulo')
    {{ trans('formauth.reset.title') }}
@endsection
@section('cabecera')
    {{ trans('formauth.reset.titlesub') }}    
@endsection
@section('errores')
     @if (Session::has('message'))
                        <div class="alert alert-warning">{{ Session::get('message') }}</div>
                    @endif 
                    @if($errors->all())
                                <div class="alert alert-danger" style="text-align: left;" id='diverror'>
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <h4>Error!</h4>
                                      {{ HTML::ul($errors->all()) }}
                                </div>
                    @endif 
                   {{-- $error --}}
                   {{-- $status --}}
@endsection
@section('formulario')                              
    {{-- Form::open(array('url' => 'password/postReset','role'=>'form' ,'class'=>'form-horizontal')) --}}
    {!! Form::open(['route' => 'password/postReset', 'class' => 'form-horizontal']) !!}
                 {!! Form::hidden('token', $token) !!}
                <div class="form-group">
                    <!--input type="password" class="form-control" placeholder="Contraseña" required=""-->
                     {{ Form::text('email', Input::old('email'), array('class' => 'form-control','placeholder'=>trans('formauth.label.email'),'required'=>'', 'autofocus'=>'')) }}
                </div>
                <div class="form-group">                    
                     {{ Form::password('password',array('class' => 'form-control','placeholder'=>trans('formauth.label.password'),'required'=>'')) }}
                </div>
                 <div class="form-group">                    
                     {{ Form::password('password_confirmation',array('class' => 'form-control','placeholder'=>trans('formauth.label.password_confirmation'),'required'=>'')) }}
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">{{ trans('formauth.reset.submit') }}</button>
                <a href="{{ URL::to('auth/login') }}"><small class="Opass">{{ trans('formauth.login.inicio') }}</small></a><br>
                <!--p class="text-muted text-center"><small>No tiene una cuenta?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Crear una cuenta</a-->
            <!--/form-->
                 {{ Form::close() }}
@endsection