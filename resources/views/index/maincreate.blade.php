<?php
    if(!isset($nobloqueo))
        Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo')  {{ $textos[0] }} @stop
@section ('contenido')
<h1> 
    @if($textos[2]=="crear")
        Nuevo Registro - {{ $textos[0] }}
    @else
        Editar Registro - {{ $textos[0] }}
    @endif
</h1>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                            @if($errors->all())
                                <script>    
                            $(document).ready(function() {
                            //    $.notify("Error al guardar!","error");
                            toastr["error"]("{{ HTML::ul($errors->all()) }}");    
                            });
                            </script>
                                <div class="alert alert-danger" style="text-align: left;" id='diverror'>
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <h4>Error!</h4>
                                      {{ HTML::ul($errors->all()) }}
                                </div>
                            @endif                        
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="{{ URL::to($textos[1]) }}">Actualizar</a>
                                </li>
                                <li><a href="{{ URL::to($textos[1].'/create') }}">Nuevo</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="">
                       @if($textos[1]!="marcaciones")
                        <a  href="{{ URL::to($textos[1]) }}" class="btn btn-default">Todos</a>
                        @endif
                       <a  href="{{ URL::to($textos[1].'/create') }}" class="btn btn-primary ">Nuevo</a>
                      </div>
@if($textos[2]=="crear")    
    {{ Form::open(array('url' => $textos[1],'role'=>'form' ,'class'=>'form-horizontal')) }}
@else
    {{ Form::model($tabla, array('route' => array($textos[1].'.update', $tabla->id), 'method' => 'PUT','class'=>'form-horizontal')) }}
@endif
    <?php
        $campos = $camposfield;
        $detalle = $camposcaption;        
        //$placeholder = array('Trámite Ejem: Bienes Mostrencos','Monto Ejem: 5');
        $total = count($campos);
        //if(count($campos) == count($detalle) && count($campos) == count($placeholder))
        if(count($campos) == count($detalle))
        {
            for($i=0;$i<$total;$i++)
            {
             echo '<div class="form-group">'.
                    Form::label($campos[$i],$detalle[$i].':',array("class"=>"col-lg-3 control-label"));
             echo '<div class="col-lg-5">';
             if(!count($campostype))
             {
                echo Form::text($campos[$i], Input::old($campos[$i]), array('class' => 'form-control','placeholder'=>'Ingrese '.$detalle[$i])); 
             }else{
                 if($campostype[$i]=="date"){             
                    echo Form::input('date',$campos[$i], $fecha[$i], ['class' => 'form-control', 'placeholder' => 'Date']);                 
                }elseif($campostype[$i]=="select"){                    
                    if($multiple[$i]=='multiple'){
                        if(isset($select))
                            echo Form::select($campos[$i]."[]" ,$lista[$i],$select[$i], array('class' => "form-control ".$clase[$i],$multiple[$i]));
                        else
                            echo Form::select($campos[$i]."[]" ,$lista[$i],Input::old($campos[$i]), array('class' => "form-control ".$clase[$i],$multiple[$i]));
                    }else {
                        echo Form::select($campos[$i] ,[null=>'Escoja...']+$lista[$i],Input::old($campos[$i]), array('class' => "form-control ".$clase[$i],$multiple[$i]));
                    }
                }elseif($campostype[$i]=="password" ){//|| $campostype[$i]=='password_confirmation'){
                    echo Form::password($campos[$i],array('class' => 'form-control','placeholder'=>'Ingrese '.$detalle[$i]));
                }elseif($campostype[$i]=="textdisabled"){
                    echo Form::text($campos[$i], $valor, array('class' => 'form-control','placeholder'=>'Ingrese '.$detalle[$i],'readonly'));
                }else{
                    echo Form::text($campos[$i], Input::old($campos[$i]), array('class' => 'form-control','placeholder'=>'Ingrese '.$detalle[$i]));
                }
                     //array("class" => "form-control","placeholder"=>"Ejem: 1 2 3")            
            }// Fin Else de campostye
            echo '</div>';
            echo '</div>';
            }//
        }
    ?>     @if($textos[1]!="marcaciones")
                {{ Form::submit("Guardar", array('class' => 'btn btn-primary')) }}
           @else
                {{ Form::submit("Consultar", array('class' => 'btn btn-primary')) }}
           @endif
{{ Form::close() }}

                        
                </div> <!-- ibox-content -->
             </div> <!-- ibox float-e-margins -->
             
             
  <script>
    $(document).ready(function() {  
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
     }); 
  </script>
  
  
        
           
       

              
     
@stop