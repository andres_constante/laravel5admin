<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $textos[0] }} @stop
@section ('scripts')
 <script>
        $(document).ready(function() {
            $('#tbbuzon').dataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]])
                    
                  
            });
        }); 
    $(document).ready(function() {
        $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)}); 
});       
</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')
<h1> {{ $textos[0] }}</h1>
                <div class="ibox float-e-margins">
                    <div class="ibox-title"> 
    @if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
	   <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif                          
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="{{ URL::to($textos[1]) }}">Listar Todos</a>
                                </li>
                                <li><a href="{{ URL::to($textos[1]."/create") }}">Nuevo</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="">
                       <a  href="{{ URL::to($textos[1]) }}" class="btn btn-primary ">Todos</a>
                       <a  href="{{ URL::to($textos[1]."/create") }}" class="btn btn-default ">Nuevo</a>
                      </div>
<table id="tbbuzon" class="table table-striped table-bordered table-hover display">
                    <thead>
                        <tr>           
                           <th>ID</th>
                           @foreach($camposcaption as $key => $value)
                           <th>{{ $value }}</th>
                           @endforeach
                            <th>Acción</th>
                    </tr>
                    </thead>
                    <tfoot>
                        <tr>            
                          <th>ID</th>
                           @foreach($camposcaption as $key => $value)
                           <th>{{ $value }}</th>
                           @endforeach
                            <th>Acción</th>
                    </tr>
                    </tfoot>                    
                    <tbody>
                @foreach($tabla as $key => $value)
		<tr>
                     <td>{{ $value->id }} </td>	
                    @foreach($camposfield as $keycam => $valuecam)
                           <td><?php 
                                $cadena="echo trim(\$value->".trim($valuecam).");";
                                eval($cadena);
                                ?>
                           </td>
                           @endforeach
                    <td>
                        <a href="{{ URL::to($textos[1]."/".$value->id."/edit") }}"><i class="fa fa-pencil-square-o"></i></a>
                        <a href="{{ URL::to($textos[1]."/".$value->id) }}" class="divpopup" target="_blank"><i class="fa fa-newspaper-o"></i></a>

                        <!--a href="#"><i class="fa fa-trash"></i> </a-->
                    </td>
		</tr>
                @endforeach
                    </tbody>
                   
                    </table>
                        {{-- $tabla->links() --}}
                </div> <!-- ibox-content -->
             </div> <!-- ibox float-e-margins -->
     
@stop