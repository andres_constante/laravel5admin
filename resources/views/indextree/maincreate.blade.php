<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo')  {{ $textos[0] }} @stop
@section('estilos')
 {{ HTML::style('css/plugins/jsTree/style.css') }}
 <style>
    .jstree-open > .jstree-anchor > .fa-folder:before {
        content: "\f07c";
    }

    .jstree-default .jstree-icon.none {
        width: 0;
    }
</style>

@stop
@section ('scripts')
{{ HTML::script('js/plugins/jsTree/jstree.min.js') }}
{{-- HTML::script('js/plugins/jsTree/jstree.checkbox.js'); --}}

<script>
function cargardatos(div,url,datos)
{
    $(div).html("Espere...");
	$.ajax({
			type: "GET",
			url: url+datos+"/edit",
			data: datos,
			error: function(objeto, quepaso, otroobj){
				//alert(quepaso);
                                $(div).html(quepaso);
			},				
			success: function(datos){
		   		//alert(datos);
				$(div).html("<strong>Permisos Asignados</strong><br>"+datos);
                                /*.on('loaded.jstree', function() {
                                    // Do something here...
                                    //alert("Listo");
                                })*/
                                 $('#jstree').on('changed.jstree', function (e, data) {
                                    var i, j, r = [];
                                    for(i = 0, j = data.selected.length; i < j; i++) {
                                        r.push(data.instance.get_node(data.selected[i]).text);
                                        //r.push(data.instance.get_node(data.selected[i]).text.)
                                    }
                                    //$('#event_result').html('Selected: ' + r.join(', '));
                                    var valor = r.join(', ');
                                    valor= valor.replace(/idmenu/g,'idmenumain[]');
                                    //valor= valor.replace(/hidden/g,'text');
                                    $('#divselect').html(valor);
                                    //            alert (valor);
                                }).jstree({
                                    'state': {
                                        'opened': true
                                        },
                                    'core' : {
                                        'check_callback' : true
                                    },
                                    "checkbox" : {
                                        "keep_selected_style" : false
                                    },
                                    "plugins" : [ "checkbox" ]                                      
                                }) ;

		  },
  			statusCode: {
			    404: function() {
				      //alert('No Existe URL');
                                      $(div).html(":(");
		    }	
			}	  
	});
}
</script>
@stop
@section ('contenido')
<h1> 
Asignación - {{ $textos[0] }}
</h1>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        
                            @if($errors->all())
                                <script>    
                            $(document).ready(function() {
                            //    $.notify("Error al guardar!","error");
                            toastr["error"]("{{ HTML::ul($errors->all()) }}");    
                            });
                            </script>
                                <div class="alert alert-danger" style="text-align: left;" id='diverror'>
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <h4>Error!</h4>
                                      {{ HTML::ul($errors->all()) }}
                                </div>
                            @endif                        
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="{{ URL::to($textos[1]) }}">Actualizar</a>
                                </li>
                                <li><a href="{{ URL::to($textos[1].'/create') }}">Modificar</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="">
                       @if($textos[1]!="marcaciones")
                        <a  href="{{ URL::to($textos[1]) }}" class="btn btn-default">Todos</a>
                        @endif
                       <a  href="{{ URL::to($textos[1].'/create') }}" class="btn btn-primary ">Modificar</a>
                      </div>
@if($textos[2]=="crear")    
    {{ Form::open(array('url' => $textos[1],'role'=>'form' ,'class'=>'form-horizontal')) }}
@else
    {{ Form::model($tabla, array('route' => array($textos[1].'.update', $tabla->id), 'method' => 'PUT','class'=>'form-horizontal')) }}
@endif

<?php
        $campos = $camposfield;
        $detalle = $camposcaption;        
        //$placeholder = array('Trámite Ejem: Bienes Mostrencos','Monto Ejem: 5');
        $total = count($campos);
        //if(count($campos) == count($detalle) && count($campos) == count($placeholder))
        if(count($campos) == count($detalle))
        {
            for($i=0;$i<$total;$i++)
            {
             echo '<div class="form-group">'.
                    Form::label($campos[$i],$detalle[$i].':',array("class"=>"col-lg-3 control-label"));
             echo '<div class="col-lg-5">';
             if(!count($campostype))
             {
                echo Form::text($campos[$i], Input::old($campos[$i]), array('class' => 'form-control','placeholder'=>'Ingrese '.$detalle[$i])); 
             }else{
                echo Form::select($campos[$i] ,[null=>'Escoja...']+$lista[$i],Input::old($campos[$i]), 
                                array('class' => "form-control","onchange"=>'cargardatos("#event_result","'.URL::to($textos[3]).'/",this.form.'.$campos[$i].'.value)'));
                     //array("class" => "form-control","placeholder"=>"Ejem: 1 2 3")            
            }// Fin Else de campostye
            echo '</div>';
            echo '</div>';
            }//
        }
    ?>   
<div id="divselect" style="display: none">
    
</div>
{{ Form::submit('Grabar Permisos!', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}
<div id="event_result">
    
</div> 


                    </div> <!-- ibox-content -->
             </div> <!-- ibox float-e-margins -->
     
@stop