<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $textos[0] }} @stop
@section ('scripts')
 <script>
        $(document).ready(function() {
            $('#tbbuzon').dataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]])
                    
                  
            });
        }); 
    $(document).ready(function() {
        $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)}); 
        $("#btnGuardar").click(function(){
            var fila=1;
            var filacual;
            $('#tbbuzon tbody tr').each(function () {
            var id = $(this).find("td").eq(0).html();
            filacual=$(this);
            //alert(id);
            //
            var index=0;
            $(this).find('td').each(function(){
        //do your stuff, you can use $(this) to get current cell
            var valor = $(this).find("input").eq(0).val();
            if(index>1 && index <6)
            {
                valor= parseFloat(valor);
                alert("Fila: "+fila+"Columna:" + index +"="+valor);                
                idtxt=index-1;
                $("#nota"+idtxt).val(valor);
            }
            index++;
        }); 
            //suma= suma + parseFloat($(this).find("td").eq(7).html());
             $( '#frmGuardar' ).on( 'submit', function(e) {
                e.preventDefault();                
              var nota1 = $('#nota1').val();
              var nota2 = $('#nota2').val();
              var nota3 = $('#nota3').val();
              var nota4 = $('#nota4').val();             
              $.ajax({
                    type: "POST",
                    url: host+'/comment/add',
                    data: {name:name, message:message, post_id:postid}
                    success: function( msg ) {
                    alert( msg );
                    }
                });
           });
            //Fin Ajax            
            fila++;
            filacual.attr( "class","success" );
        });
        });
});
function solonumeros(e){
//	alert('Andres');
  	tecla = (document.all) ? e.keyCode : e.which;
    //patron =/[0-9]/;
    patron =/^([0-9])*[.]?[0-9]*$/;
    te = String.fromCharCode(tecla);
	return patron.test(te);
}
function validarnota(nota)
{
	no=parseInt(nota.value);
	if(no>10)
	{
		alert("La nota ingresada es mayor que 10");
		nota.select();
		nota.focus();
		return false;
	}
}
function sumano(tr)
{    var suma=0;
        var index=0;
        $(tr).find('td').each(function(){
        //do your stuff, you can use $(this) to get current cell
            var valor = $(this).find("input").eq(0).val();
            if(index>1 && index <6)
            {
                valor= parseFloat(valor);
                if(!isNaN(valor))
                    suma= suma + valor;
            }else if(index==6){
               $(this).find("input").eq(0).val(suma); 
            }else if(index==7){
               $(this).find("input").eq(0).val(suma/4); 
            }
            index++;
        }); 
        //alert(suma);
  /*
        $('#tbbuzon tr').each(function(){
        $(tr).find('td').each(function(){
        //do your stuff, you can use $(this) to get current cell
        })
    });
*/
        /*$('#tbbuzon tbody tr').each(function () {
            var id = $(this).find("td").eq(0).html();
            suma= suma + parseFloat($(this).find("td").eq(7).html());
        });*/    
return;
//	alert("tqm");
	no1=parseInt(nota1.value);
	no2=parseInt(nota2.value);
	no3=parseInt(nota3.value);
	if(isNaN(no1))	
		no1=0;
	if(isNaN(no2))	
		no2=0;
	if(isNaN(no3))	
		no3=0;

	if(no1>20 || no2>20 || no3>20)
		return false;
	toto.value=no1+no2+no3;
	tota=parseInt(toto.value);	
	if(isNaN(tota))	
		tota=0;
	suple=parseInt(rec.value);	
	if(isNaN(suple))	
		suple=0;
}

</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')
<h1> {{ $textos[0] }}</h1>
                <div class="ibox float-e-margins">
                    <div class="ibox-title"> 
    @if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
	   <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif                          
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="{{ URL::to($textos[1]) }}">Listar Todos</a>
                                </li>                                
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="">
                       <a  href="{{ URL::to($textos[1]) }}" class="btn btn-primary ">Todos</a>
                       <a  href="javascript::" id="btnGuardar" class="btn btn-success ">Guardar</a>
                      </div>
{{ Form::open(array('url' => $textos[1],'role'=>'form' ,'class'=>'form-horizontal')) }}
<table id="tbbuzon" class="table table-striped table-bordered table-hover display">
                    <thead>
                        <tr>           
                           <th>ID</th>
                           @foreach($camposcaption as $key => $value)
                           <th>{{ $value }}</th>
                           @endforeach                            
                    </tr>
                    </thead>
                    <tfoot>
                        <tr>            
                          <th>ID</th>
                           @foreach($camposcaption as $key => $value)
                           <th>{{ $value }}</th>
                           @endforeach
                            
                    </tr>
                    </tfoot>                    
                    <tbody>
                        <?php $fil=0; ?>
                @foreach($tabla as $key => $value)
		<tr  <?php echo "id='tr-$fil'"; ?> >
                     <td>{{ $value->id }} </td>	
                     <?php $in=0; ?>
                    @foreach($camposfield as $keycam => $valuecam)
                           <td><?php
                                if($campostype[$in]=="text"){
                                    $cadena="echo trim(\$value->".trim($valuecam).");";
                                }elseif($campostype[$in]=="input"){
                                    $onkeyup="'onkeyup'=>'validarnota(this);sumano(\"#tr-$fil\")'";
                                    $cadena="echo Form::text('".$valuecam."-".$in."', \$value->".trim($valuecam).", array('class' => '','style'=>'width:40px; text-align: right;','placeholder'=>'Ingrese ".$camposcaption[$in]."','onkeypress'=>'return solonumeros(event)',$onkeyup));";
                                    //die($cadena);
                                }elseif($campostype[$in]=="input-disabled"){
                                    $cadena="echo Form::text('".$valuecam."-".$in."', \$value->".trim($valuecam).", array('class' => '','style'=>'width:40px; text-align: right;','readonly','placeholder'=>'Ingrese ".$camposcaption[$in]."'));";
                                    //die($cadena);
                                }                                
                                eval($cadena);
                                $in++;
                                ?>
                           </td>
                           @endforeach
		</tr>
                <?php $fil++; ?>
                @endforeach
                    </tbody>
                   
                    </table>
{{ Form::close() }}
<div id="divformulario">
{{ Form::open(array('id'=>'frmGuardar','url' => $textos[1],'role'=>'form' ,'class'=>'form-horizontal')) }}

    {{ Form::text('nota1', Input::old('nota1'), array('id'=>'nota1','class' => 'form-control','placeholder'=>'Ingrese Nota1')) }}
    {{ Form::text('nota2', Input::old('nota2'), array('id'=>'nota2','class' => 'form-control','placeholder'=>'Ingrese Nota2')) }}
    {{ Form::text('nota3', Input::old('nota3'), array('id'=>'nota3','class' => 'form-control','placeholder'=>'Ingrese Nota3')) }}
    {{ Form::text('nota4', Input::old('nota4'), array('id'=>'nota4','class' => 'form-control','placeholder'=>'Ingrese Nota4')) }}

{{ Form::close() }}
</div>
                        {{-- $tabla->links() --}}
                </div> <!-- ibox-content -->
             </div> <!-- ibox float-e-margins -->
     
@stop